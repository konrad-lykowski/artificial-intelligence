import warnings

from asl_data import SinglesData


def recognize(models: dict, test_set: SinglesData):

    warnings.filterwarnings("ignore", category=DeprecationWarning)
    probabilities = []
    guesses = []

    for word, (x, length) in test_set.get_all_Xlengths().items():
        best_score = float("-inf")
        best_word = ""
        probabilities_instance = {}
        for word_model, model in models.items():
            try:
                score = model.score(x, length)
                probabilities_instance[word_model] = score
            except:
                probabilities_instance[word_model] = float("-inf")
                score = float("-inf")

            if score > best_score:
                best_score = score
                best_word = word_model

        probabilities.append(probabilities_instance)
        guesses.append(best_word)

    return probabilities, guesses
